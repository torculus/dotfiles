Set-PSReadLineOption -EditMode Vi

# starship prompt
#Invoke-Expression (&starship init powershell)

# setup mcfly
$Env:MCFLY_KEY_SCHEME = 'vim'
Invoke-Expression -Command $(mcfly init powershell | out-string)
