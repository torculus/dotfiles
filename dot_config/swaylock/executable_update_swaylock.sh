#!/usr/bin/env bash
if [ -e ~/.config/swaylock/swaylockimg ]
then
    rm ~/.config/swaylock/swaylockimg
fi

#swaylockinp=$(grep -o "\"/.*.\"" ~/.azotebg | sed s/\"//g)
swaylockinp=$(swww query | cut -d ":" -f 5- | xargs)

# requires imagemagick
magick "$swaylockinp" -scale 10% -blur 0x2.5 -resize 1000% ~/.config/swaylock/swaylockimg
