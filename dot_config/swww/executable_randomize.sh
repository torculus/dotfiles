#!/bin/bash

export SWWW_TRANSITION_FPS=60
export SWWW_TRANSITION_STEP=2
export SWWW_TRANSITION='random'

# This controls (in seconds) when to switch to the next image
INTERVAL=300

while true;
do
    month=$(date +%b)
    mon_num=$(date +%m)

    # This needs to be an integer for comparison purposes
    declare -i hour24=$((10#$(date +%H)))

    if [[ $hour24 -ge 7 && $hour24 -lt 20 ]]; then
      type=light
    else
      type=dark
    fi

    case $month in
      Dec | Jan | Feb)
	season=Winter
	;;

      Mar | Apr | May)
	season=Spring
	;;

      Jun | Jul | Aug)
	season=Summer
	;;

      *)
	season=Fall
	;;
    esac

    if [[ -d "$HOME/Pictures/Wallpapers/$type/$season/$mon_num-$month/" ]]; then
      wallDir="$HOME/Pictures/Wallpapers/$type/$season/$mon_num-$month/"
    else
      wallDir="$HOME/Pictures/Wallpapers/$type/$season/"
    fi

    swww img "$(find -L "$wallDir" -type f \( -name "*.jpg" -o \
    	-name "*.png" -o -name "*.avif" \) -exec shuf -e -n1 {} +)";

    # eval instead of exec because we don't want to kill the current shell!
    eval $HOME/.config/swaylock/update_swaylock.sh;
    sleep $INTERVAL;
done
